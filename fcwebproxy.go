package main

import (
	"encoding/binary"
	"encoding/json"
	"flag"
	"io"
	"net"
	"os"

	log "github.com/golang/glog"
	"golang.org/x/net/websocket"
)

const (
	capstr = "+Freeciv.Web.Devel-3.1"
)

var (
	users = flag.String("users", "", "json file containing user database")
	tileData = flag.String("tiles", "", "json file containing map data")
	usermap = make(map[string]serverinfo)
	savedTiles []map[string]interface{}
)

type serverinfo struct {
	Server, Token string
	Port int
}

func handleConnection(conn net.Conn) {
	defer conn.Close()

	var plen int16
	if err := binary.Read(conn, binary.BigEndian, &plen); err != nil {
		log.Error(err)
		return
	}
	lr := io.LimitReader(conn, int64(plen-2))
	buf := make([]byte, plen-2)
	if _, err := io.ReadFull(lr, buf); err != nil {
		log.Error(err)
		return
	}

	buf = buf[:len(buf)-1]

	var p map[string]interface{}
	if err := json.Unmarshal(buf, &p); err != nil {
		log.Error(err)
		return
	}

	username, ok := p["username"].(string)
	if !ok {
		log.Error("expect login packet")
		return
	}
	si, ok := usermap[username]
	if !ok {
		log.Error("user not found")
		return
	}

	p["port"] = si.Port
	p["password"] = si.Token
	p["capability"] = capstr

	var err error
	buf, err = json.Marshal(p)
	if err != nil {
		log.Error(err)
		return
	}

	ws, err := websocket.Dial(si.Server, "", "https://www.freecivweb.org")
	if err != nil {
		log.Error(err)
		return
	}
	defer func() {
		ws.Close()
	}()

	if _, err := ws.Write(buf); err != nil {
		log.Error(err)
		return
	}
	log.Infof("connected to %s as %s", si.Server, username)

	seenTiles := make(map[int]bool)


	// Read from civclient.
	go func() {
		defer func() {
			ws.Close()
		}()
		for {
			var plen int16
			if err := binary.Read(conn, binary.BigEndian, &plen); err != nil {
				log.Error(err)
				return
			}
			lr := io.LimitReader(conn, int64(plen-2))
			buf := make([]byte, plen-2)
			if _, err := io.ReadFull(lr, buf); err != nil {
				log.Error(err)
				return
			}

			buf = buf[:len(buf)-1]
			if _, err := ws.Write(buf); err != nil {
				log.Error(err)
				return
			}
		}
	}()

	dec := json.NewDecoder(ws)

	sentSavedMap := false

	sendBinPacket := func(packet interface{}) error {
		b, err := json.Marshal(packet)
		if err != nil {
			return err
		}
		if err := binary.Write(conn, binary.BigEndian, int16(len(b)+3)); err != nil {
			return err
		}
		if _, err := conn.Write(b); err != nil {
			return err
		}
		if _, err := conn.Write([]byte{0}); err != nil {
			return err
		}
		return nil
	}

	for {
		// read open bracket
		_, err := dec.Token()
		if err != nil {
			log.Error(err)
			return
		}
		for dec.More() {
			var p map[string]interface{}
			err := dec.Decode(&p)
			if err != nil {
				log.Error(err)
				return
			}
			switch pid := int(p["pid"].(float64)); pid {
			// tile_info
			case 15:
				seenTiles[int(p["tile"].(float64))] = true
			// spaceship_info, inject saved maps once
			case 137:
				if sentSavedMap {
					break
				}
				sentSavedMap = true
				for _, ti := range savedTiles {
					if seenTiles[int(ti["tile"].(float64))] {
						continue
					}
					if err := sendBinPacket(ti); err != nil {
						log.Error(err)
						return
					}
				}
			}

			if err := sendBinPacket(p); err != nil {
				log.Error(err)
				return
			}
		}
		// read closing bracket
		_, err = dec.Token()
		if err != nil {
			log.Error(err)
			return
		}
	}
}

func main() {
	flag.Parse()

	u, err := os.Open(*users)
	if err != nil {
		log.Exit(err)
	}
	if err := json.NewDecoder(u).Decode(&usermap); err != nil {
		log.Exit(err)
	}
	u.Close()

	if *tileData != "" {
		td, err := os.Open(*tileData)
		if err != nil {
			log.Exit(err)
		}
		if err := json.NewDecoder(td).Decode(&savedTiles); err != nil {
			log.Exit(err)
		}
		td.Close()
	}

	ln, err := net.Listen("tcp", ":5566")
	if err != nil {
		// handle error
	}
	for {
		conn, err := ln.Accept()
		if err != nil {
			// handle error
		}
		go handleConnection(conn)
	}
}
